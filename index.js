const express = require("express")

const app = express()
const port = 3000

app.use(express.json())

app.use(express.urlencoded({extended:true}))

app.get("/home",(req,res)=>{
    res.send("Welcome to the Home Page")
})

let users=[
    {
        username:"johndoe",
        password:"johndoe1234"
    },
    {
        username:"xavier",
        password:"123"
    },
    {
        username:"levpogi",
        password:"444"
    },
    {
        username:"janesmith",
        password:"janesmith1234"
    }
]

app.get("/users",(req,res)=>{
    
    if(users.length !== 0){
        res.send(users)
    }else{
        res.send("No existing user")
    }
})
app.delete("/delete-users",(req,res)=>{
    let message
 
    console.log(users)

    for(let i = 0; i<users.length;i++){
        if(req.body.username == users[i].username && req.body.password == users[i].password){
            users.splice(i,1)
            message = `User ${req.body.username}'s has been deleted.`
            break
        }else{
            message="User does not exist"
        }
    }

    res.send(message)
    console.log(users)
})

app.listen(port,()=> console.log(`Server running at port ${port}`))